#include "cliente.hpp"
#include <iostream>

using namespace std;

Cliente::Cliente(string nome, string nascimento, string cpf, string telefone, string cep, string email, int sociedade){
    cout << "Construtor da Classe Cliente" << endl;
    set_nome(nome);
    set_nascimento(nascimento);
    set_cpf(cpf);
    set_telefone(telefone);
    set_cep(cep);
    set_email(email);
    set_sociedade(sociedade);
    cout << "";
}

Cliente::~Cliente(){
    cout << "Destrutor da Classe Cliente" << endl;
}

string Cliente::get_nome(){
    return nome;
}

void Cliente::set_nome(string nome){
    this->nome = nome;
}

string Cliente::get_nascimento(){
    return nascimento;
}

void Cliente::set_nascimento(string nascimento){
    this->nascimento = nascimento;
}

string Cliente::get_cpf(){
    return cpf;
}

void Cliente::set_cpf(string cpf){
    this->cpf = cpf;
}

string Cliente::get_telefone(){
    return telefone;
}

void Cliente::set_telefone(string telefone){
    this->telefone = telefone;
}

string Cliente::get_cep(){
    return cep;
}

void Cliente::set_cep(string cep){
    this->cep = cep;
}

string Cliente::get_email(){
    return email;
}

void Cliente::set_email(string email){
    this->email = email;
}

int Cliente::get_sociedade(){
    return sociedade;
}

void Cliente::set_sociedade(int sociedade){
    this->sociedade = sociedade;
}

void Cliente::imprime_cliente(){
    cout << "Produto: " << get_nome() << endl;
    cout << "Data de nascimento: " << get_nascimento() << endl;
    cout << "CPF: " << get_cpf() << endl;
    cout << "Telefone: " << get_telefone() << endl;
    cout << "CEP: " << get_cep() << endl;
    cout << "E-mail: " << get_email() << endl;
    cout << "Cliente Sócio? " << get_sociedade() << endl;
}