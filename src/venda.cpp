#include "venda.hpp"
#include <fstream>
#include <iostream>

Venda::Venda(int codigo, string nome, string categoria, int estoque, float preco){
    set_codigo(codigo);
    set_nome(nome);
    set_categoria(categoria);
    set_estoque(estoque);
    set_preco(preco);
    cout << "";
} 

Venda::~Venda(){
    cout << "Destrutor da Classe Venda" << endl;
}

void Venda::set_preco(float preco){
    this->preco = preco;}

 

float Venda::get_preco() 
{

    return preco;

}

int atualiza_estoque(int quantidade, int estoque){

    if (quantidade > estoque){
        cout << "Esse item não está disponível no estoque" << endl;
        return 0;
    }
    else{
        estoque -=quantidade;
    }
    return estoque;
}

void Venda::imprime_estoque(){

    cout << "Produto: " << get_nome() << endl;
    cout << "Categoria: " << get_categoria() << endl;
    cout << "Preço: " << get_preco() << endl;
    cout << "Estoque: " << get_estoque() << endl;

}


