#include <iostream>
#include "estoque.hpp"
#include "venda.hpp"
#include "cliente.hpp"
#include <string>
#include <vector>
#include <fstream>
#include <cstdlib>

using namespace std;


vector<Venda * > stock;
vector<Estoque * > stc ;
vector<Cliente * > cliente;
int codigo;
int estoque;

string getString(){
    string valor;
    getline(cin, valor);
    return valor;
}

template <typename T1>

T1 getInput(){
    while(true){
    T1 valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}

void checkout(int desconto){
    float total =0;
    int quantidade;
    int input;
    do{
        codigo =0;
        cout << "Digite o código do produto ou 0. Sair." << endl;
        cin >> input;
        if (input == 0){
            break;
        }
        for (Venda * c: stock){
            if (c->get_codigo() == input)
            {
                //codigo = p;
                break;
            }
        }
        cout << "Quantidade: " << endl;
        cin >> quantidade;
        total += quantidade * stock[codigo]->get_preco();
       // stock[codigo]->atualiza_estoque(quantidade, estoque);
    }
    while(input != 0);
    cout << "Total = $" << total << endl;
    if (desconto == 1){
        total = total * 0.75;
        cout << "Sua compra com desconto de 15% tem um Total = $" << total << endl;
    }
}

void cadastrar_produto(){
    std::ofstream saida;
    std::ifstream entrada;
    string nome;
    string categoria;
    float preco;
    saida.open("novo_estoque.txt", std::ios_base::app);

    cout << "Código: ";
    estoque = getInput<int>();
    cout << "Nome: ";
    nome = getString(); 
    cout << "Categoria: ";
    categoria = getString();
    cout << "Quantidade: ";
    estoque = getInput<int>();
    cout << "Preço: ";
    preco = getInput<float>();

    if(saida.is_open()){
        saida << codigo << " " << nome << " " << categoria << " " << estoque << " " << preco << endl; 
        cout << "-> ";
        saida.close();
  }
    stock.push_back(new Venda(codigo, nome, categoria, estoque, preco));
    stc.push_back(new Venda(codigo, nome, categoria, estoque, preco));
    }

void cadastrar_cliente(){
    std::ofstream saida;
    std::ifstream entrada;
    string nome;
    string nascimento;
    string cpf;
    string telefone;
    string cep;
    string email;
    int sociedade;
    saida.open("novo_cliente.txt", std::ios_base::app);
    cout << "Nome: ";
    nome = getString(); 
    cout << "Data de nascimento: ";
    nascimento = getString();
    cout << "CPF: ";
    cpf = getString();
    cout << "Telefone: ";
    telefone = getString();
    cout << "CEP: ";
    cep = getString();
    cout << "Email: ";
    email = getString();
    cout << "É Sócio?(1) Sim (2) Não ";
    sociedade = getInput<int>();
    if(saida.is_open()){
        saida << nome << " " << nascimento << " " << cpf << " " << telefone << " " << cep << " " << email << " " << sociedade << endl; 
    cout << "-> ";;
        saida.close();
  }
    cliente.push_back(new Cliente(nome, nascimento, cpf, telefone, cep, email, sociedade));
    }

void listar_estoque(){
    std::ofstream saida;
    std::ifstream entrada;
    entrada.open("novo_estoque.txt");
    string nome;
    string categoria;
    float preco;
    if(entrada.is_open()){
        while(entrada >> codigo >> nome >> categoria >> estoque >> preco){
            cout << codigo << " " << nome << " " << categoria << " " << estoque << " " << preco << endl;
        } 
        cout << endl;
        entrada.close();
  }
  }

int recomendar_produto(){
    return 0;
}

int main(){
    
    cout << "";
    int desconto;
    int comando = -1;
    int com_venda = -1;
    int com_estoque = -1;
    int com_recomenda = -1;

    while(comando != 0){
            menu:
        cout<<"\n\n\n\n\n\n\n\n\n\n\n \t\t\t|============ V DE VEGANO  ============|";
        cout<<"\n\t\t      SISTEMA DE GESTÃO DE LOJA \n";
        cout<<"=============================================================";
        cout<<"\n\n\t\t   1. Menu de Venda\n\n\t\t   2. Menu de Estoque\n\n\t\t   3. Menu de Recomendação";
        cout<<"\n\n=============================================================\n";
        cout<<"\n\nInsira sua escolha:";
        comando = getInput<int>();

        switch(comando){
            case 1:
            while(com_venda != 0){
                cout<<"=================================================================";
                cout<<"\n\n\t\t\t    MENU DE VENDA\n1. Pesquisar Cliente\n2. Sair";
                cout<<"\n\n\n==========================END OF MENU=============================";
                cout<<"\n\n Insira sua escolha :\t";
                com_venda = getInput<int>();
                switch(com_venda){
                    case 1:
                        long int cpf;
                        int enter_cpf;
                        cout << "Digite o cpf do cliente ou 0. pra sair" << endl;
                        cin >> enter_cpf;
                        if (enter_cpf == 0){
                        break;
                    }
                        else{
                            string line;
                            ifstream myfile("novo_cliente.txt"); //open file
	                        getline(myfile, line); 
	                        {
		                    bool found = false;
		                    while(getline( myfile, line ) && !found)
			                {
			                    if(line.find(cpf) != string::npos) 
				                { 											
					                found = true;
				                }
			                }

		                if (!found){    
		                    cout << "\n\n   Cliente não encontrado" << endl; } 
                            }
                            }
                        break;
                    case 2:
                        goto menu;
                    default:
                        cout << "Opção inválida!" << endl;        
                }

            }
                break;
            case 2:
            while(com_estoque != 0){
                cout<<"=================================================================";
                cout<<"\n\n\t\t\t    MENU DE ESTOQUE\n1. Cadastrar Produto\n2. Listar Estoque\n3. Sair";
                cout<<"\n\n\n==========================END OF MENU=============================";
                cout<<"\n\n Insira sua escolha :\t";
                com_estoque = getInput<int>();
                switch(com_estoque){
                    case 1:
                        cadastrar_produto();
                        break;
                    case 2:
                        listar_estoque();
                        break;
                    case 3:
                        goto menu;
                    default:
                        cout << "Opção inválida!" << endl;        
                }}

                break;
            case 3:
            while(com_recomenda != 0){
                cout<<"=================================================================";
                cout<<"\n\n\t\t\t    MENU DE RECOMENDAÇÃO\n1. Buscar Produtos Recomendados\n2. Sair";
                cout<<"\n\n\n==========================END OF MENU=============================";
                cout<<"\n\n Insira sua escolha :\t";
                com_recomenda = getInput<int>();
                switch(com_recomenda){
                    case 1:
                        recomendar_produto();
                        break;
                    case 2:
                        goto menu;
                    default:
                        cout << "Opção inválida!" << endl;        
                }}

                break;               
            default:
                cout << "Opção inválida!" << endl;
        }
    }
    return 0;
}



