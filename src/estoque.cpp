#include "estoque.hpp"
#include <iostream>

using namespace std;

Estoque::Estoque(){
    cout << "Construtor da Classe Estoque" << endl;
    set_codigo(0);
    set_nome("");
    set_categoria("");
    set_estoque(0);
    cout << "";
}
Estoque::~Estoque(){
    cout << "Destrutor da Classe Estoque" << endl;
}

string Estoque::get_nome(){
    return nome;
}

void Estoque::set_nome(string nome){
    this->nome = nome;
}

string Estoque::get_categoria(){
    return categoria;
}

void Estoque::set_categoria(string categoria){
    this->categoria = categoria;
}

int Estoque::get_codigo(){
    return codigo;
}

void Estoque::set_codigo(int codigo){
    this->codigo = codigo;
}

int Estoque::get_estoque(){
    return estoque;
}

void Estoque::set_estoque(int estoque){
    this->estoque = estoque;
}


void Estoque::imprime_estoque(){
    cout << "Código: " << get_codigo() << endl;
    cout << "Produto: " << get_nome() << endl;
    cout << "Categoria: " << get_categoria() << endl;
    cout << "Estoque: " << get_estoque() << endl;
}