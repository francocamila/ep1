#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>

using namespace std;

class Cliente{
    private:
        string nome;
        string nascimento;
        string cpf;
        string telefone;
        string cep;
        string email;
        int sociedade;
    public:
        Cliente(string nome, string nascimento, string cpf, string telefone, string cep, string email, int sociedade);
        ~Cliente();
        string get_nome();
        void set_nome(string nome);
        string get_nascimento();
        void set_nascimento(string nascimento);
        string get_cpf();
        void set_cpf(string cpf);
        string get_telefone();
        void set_telefone(string telefone);
        string get_cep();
        void set_cep(string cep);
        string get_email();
        void set_email(string email);
        int get_sociedade();
        void set_sociedade(int sociedade);
        void imprime_cliente();
};

#endif