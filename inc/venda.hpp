#ifndef VENDA_HPP
#define VENDA_HPP

#include <string>
#include "estoque.hpp"

using namespace std;

class Venda : public Estoque{
    private:
        float preco;
    public:
        Venda(int codigo, string nome, string categoria, int estoque, float preco);
        ~Venda();
        float get_preco();
        void set_preco(float preco);
        int atualiza_estoque(int quantidade, int estoque);
        void imprime_estoque();
};

#endif