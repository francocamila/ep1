#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP

#include <string>

using namespace std;

class Estoque{
    private:
        string nome;
        string categoria;
        int codigo;
    public:
        Estoque();
        ~Estoque();
        int estoque;
        string get_nome();
        void set_nome(string nome);
        string get_categoria();
        void set_categoria(string categoria);
        int get_codigo();
        void set_codigo(int codigo);
        int get_estoque();
        virtual void set_estoque(int estoque);
        virtual void imprime_estoque();

};

#endif