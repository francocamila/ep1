Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

    -Abra o terminal;
    -Encontre o diretório raiz do projeto; 
    -Limpe os arquivos objeto: $ make clean
    -Compile o programa: $ make
    -Execute: $ make run

Quando o programa for executado ele mostratá um menu do Supermercado, então deve-se digitar a escolha de um dos Modos: Modo Venda, Modo Estoque ou Modo recomendação. Em seguida, sub-menus irão aparecer para Cadastro de Produto, Cadastro de Cliente, Listagem de estoque. As Bibliotecas utilizadas foram:

    -iostream
    -string
    -vector
    -fstream
    -cstdlib
